import React from "react";
import { Provider } from "react-redux";
//import { PersistGate } from "redux-persist/integration/react";
//import Loading from "./components/Loading";
import Main from "./components/Main";
import { store } from "./redux/configureStore";
//<PersistGate loading={<Loading />} persistor={persistor}></PersistGate>, persistor
export default function App() {
  return (
    <Provider store={store}>
      <Main />
    </Provider>
  );
}
