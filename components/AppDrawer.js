import React, { useState, useEffect } from "react";
//import { NavigationContainer } from "@react-navigation/native";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from "@react-navigation/drawer";
import { View, ScrollView, Text, Dimensions } from "react-native";
import Home from "./Home";
const Drawer = createDrawerNavigator();
import DEstDrawer from "./DatosEstudiantiles/DEstDrawer";
import Notifications from "./Notifications";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faHome,
  faBell,
  faCalendar,
  faUser,
  faCheckDouble,
} from "@fortawesome/free-solid-svg-icons";
import MiCuenta from "./DatosEstudiantiles/MiCuenta";
import Asignaciones from "./Asignacion/Main";

import CalendarComponent from "./Calendar";
import { createStackNavigator } from "@react-navigation/stack";
import { useDispatch, useSelector } from "react-redux";
import Loading from "./Loading";

import { SetMenu1, SetMenu2, GetMenu } from "../redux/actions/ActionCreators";

const screenHeight = Dimensions.get("window").height;
const Stack = createStackNavigator();

function DatosEstudiantiles() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="DatosEstudiantiles"
        component={DEstDrawer}
        options={{
          headerShown: false,
        }}
      />
      {/*<Stack.Screen
        name="Cursos"
        component={Cursos}
        options={{
          headerShown: false,
        }}
      />*/}
    </Stack.Navigator>
  );
}
function CustomDrawerContent(props) {
  const [mactive, setMactive] = useState(0);
  const dispatch = useDispatch();
  console.log("*****************************" + props.activeItemKey);
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      {/*<DrawerItem
        label="Inicio"
        name="Inicio"
        onPress={() => {
          dispatch(SetMenu1());
          props.navigation.navigate("Inicio");
        }}
      />
      <DrawerItem
        label="Datos Estudiantiles"
        onPress={() => {
          dispatch(SetMenu2());
          props.navigation.navigate("Profile");
        }}
      />*/}
    </DrawerContentScrollView>
  );
}
function MyDrawer() {
  const Menu = useSelector((state) => state.MenuReducer.Menu);
  console.log(Menu);
  //console.log(Menu.Menu);
  return (
    <Drawer.Navigator
      initialRouteName="Inicio"
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      {Menu == 2 ? (
        <Drawer.Screen
          name="Profile"
          component={MiCuenta}
          options={{
            drawerLabel: "Mi Cuenta",
            drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faUser} />,
          }}
        />
      ) : (
        () => {}
      )}
      {Menu == 1 ? (
        <Drawer.Screen
          name="Notificationes"
          component={Notifications}
          options={{
            drawerLabel: "Notificaciones",
            drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faBell} />,
          }}
        />
      ) : (
        () => {}
      )}

      {Menu == 1 ? (
        <Drawer.Screen
          name="Calendario"
          component={CalendarComponent}
          options={{
            drawerLabel: "Calendario",
            drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faCalendar} />,
          }}
        />
      ) : (
        () => {}
      )}

      {Menu == 1 ? (
        <Drawer.Screen
          name="DatosEstudiantiles"
          component={MiCuenta}
          options={{
            drawerLabel: "Datos Estudiantiles",
            drawerIcon: ({ focused }) => {
              const dispatch = useDispatch();
              dispatch(SetMenu1());
              return <FontAwesomeIcon icon={faUser} />;
            },
          }}
        />
      ) : (
        () => {}
      )}
      {Menu == 1 ? (
        <Drawer.Screen
          name="Asignaciones"
          component={Asignaciones}
          options={{
            drawerLabel: "Asignaciones",
            drawerIcon: ({ focused }) => (
              <FontAwesomeIcon icon={faCheckDouble} />
            ),
          }}
        />
      ) : (
        () => {}
      )}
      <Drawer.Screen
        name="Inicio"
        component={Home}
        options={{
          drawerLabel: "Inicio",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faHome} />,
        }}
      />
      {/*Menu == 1 ?  : () => { }*/}
    </Drawer.Navigator>
  );
}

function AppDrawer() {
  const Menu = useSelector((state) => state.MenuReducer.Menu);

  /*console.log(Menu);

  if (Menu.isLoading) {
    return <Loading />;
  } else if (Menu.errMess)
    return (
      <View>
        <Text>{this.props.dishes.errMess}</Text>
      </View>
    );
  else {*/
  //if (Menu == 1)
  //dispatch(GetMenu());
  return <MyDrawer />;
  /*else if (Menu == 2)
      //1 = datos estudiantiles
      return <DEstDrawer />;
  }*/
}

export default AppDrawer;
