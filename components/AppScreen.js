import React from "react";
import { View, ScrollView, Dimensions, StatusBar, Alert } from "react-native";
const screenHeight = Dimensions.get("window").height;
import { useIsFocused } from "@react-navigation/native";
import AppDrawer from "./AppDrawer";
const FocusAwareStatusBar = (props) => {
  const isFocused = useIsFocused();
  return isFocused ? <StatusBar {...props} /> : null;
};
function AppScreen({ MenuActive, navigation }) {
  return (
    <View style={{ height: screenHeight }}>
      <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
      <AppDrawer />
    </View>
  );
}

export default AppScreen;
