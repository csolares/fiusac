import React, { useState } from "react";
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Animated,
  View,
  Alert,
  Modal,
  TouchableHighlight,
  Dimensions,
} from "react-native";

import { Card, Header, Button, Text } from "react-native-elements";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faBars, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import HeaderNavigation from "./HeaderNavigation";
import Swipeable from "react-native-gesture-handler/Swipeable";
import { useIsFocused } from "@react-navigation/native";
const FocusAwareStatusBar = (props) => {
  const isFocused = useIsFocused();
  return isFocused ? <StatusBar {...props} /> : null;
};

var DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "First Item",
    body: "First notification",
  },
  {
    id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
    title: "Second Item",
    body: "Second notification",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d72",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d721",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d722",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d723",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d724",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d725",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d726",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d77",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
  {
    id: "58694a0f-3da1-471f-bd96-145571e29d728",
    title: "Third Item",
    body:
      "Third notificationssdafasdf \n" + "shadowOffsetasdf\n" + "asdfas" + "",
  },
];

const SCREEN_WIDTH = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const Item = ({ item, onPress }, props) => {
  const rightSwipe = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [0, 100],
      outputRange: [1, 0],
      extrapolate: "clamp",
    });
    return (
      <TouchableOpacity onPress={props.handleDelete} activeOpacity={0.6}>
        <View style={styles.deleteBox}>
          <Animated.Text style={{ transform: [{ scale: scale }] }}>
            Delete
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Swipeable renderRightActions={rightSwipe}>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.itemContainer}>
          <Text>{item.title}</Text>
        </View>
      </TouchableOpacity>
    </Swipeable>
  );
};

function Notifications({ navigation }) {
  /**
   * Modal
   */

  const [modalVisible, setModalVisible] = useState(false);

  /**
   * LISTA
   */
  const [selectedId, setSelectedId] = useState(null);
  const onPress = (id) => {
    setModalVisible(true);
    setSelectedId(id);
  };
  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "#6e3b6e" : "#f9c2ff";

    return (
      <Item
        item={item}
        onPress={() => onPress(item.id)}
        style={{ backgroundColor }}
      />
    );
  };

  return (
    <SafeAreaView>
      <FocusAwareStatusBar barStyle="dark-content" backgroundColor="#4A4138" />
      <View style={{ height: screenHeight }}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Card>
                <Card.Title>
                  {selectedId != undefined
                    ? DATA.filter(
                        (notification) => notification.id == selectedId
                      )[0].title
                    : ""}
                </Card.Title>
                <Card.Divider />
                <Text>
                  {" "}
                  {selectedId != undefined
                    ? DATA.filter(
                        (notification) => notification.id == selectedId
                      )[0].body
                    : ""}
                </Text>
              </Card>

              <TouchableHighlight
                style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
              >
                <Text style={styles.textStyle}>Cerrar</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>
        {/** 
      finaliza modal
      */}
        <HeaderNavigation navigation={navigation} />
        <ScrollView>
          <FlatList
            data={DATA}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            extraData={selectedId}
            ItemSeparatorComponent={() => {
              return <View style={styles.separatorLine}></View>;
            }}
          />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  itemContainer: {
    height: 80,
    width: SCREEN_WIDTH,
    backgroundColor: "white",
    justifyContent: "center",
    padding: 16,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  deleteBox: {
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",
    width: 100,
    height: 80,
  },
  scrollView: {
    backgroundColor: "white",
  },
  separatorLine: {
    height: 1,
    backgroundColor: "black",
  },
  title: {
    fontSize: 32,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 100,
      height: 200,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});

export default Notifications;
