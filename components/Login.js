import React, { useState } from "react";

import {
  SafeAreaView,
  View,
  Dimensions,
  Image,
  TextInput,
  Alert,
  StatusBar,
} from "react-native";

import AsyncStorage from "@react-native-async-storage/async-storage";
import * as AppAuth from "expo-app-auth";
import { Button, Text, Divider, Card, CheckBox } from "react-native-elements";
const screenHeight = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
import styles from "./styles";

import { useIsFocused } from "@react-navigation/native";
const FocusAwareStatusBar = (props) => {
  const isFocused = useIsFocused();
  return isFocused ? <StatusBar {...props} /> : null;
};

function Login({ navigation }) {
  const [isSelected, setSelection] = useState(false);
  return (
    <SafeAreaView style={{ height: screenHeight, backgroundColor: "white" }}>
      <FocusAwareStatusBar barStyle="ligth-content" backgroundColor="#4A4138" />
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          height: screenHeight,
        }}
      >
        <Image
          style={styles.loginLogo}
          source={require("../assets/img/fiusac.png")}
        />

        <Card containerStyle={styles.loginAndroidCard}>
          <Card.Title>INICIAR SESIÓN INGENIERIA USAC</Card.Title>
          <View>
            <TextInput
              placeholder="CUI / REGISTRO ACADEMICO / REGISTRO PERSONAL"
              style={styles.item2}
            />

            <Card.Divider />
            <Button
              buttonStyle={styles.loginButton}
              onPress={() => navigation.navigate("AppDrawer")}
              title="INICIAR SESIÓN"
            />
          </View>
        </Card>
      </View>
    </SafeAreaView>
  );
}

let config = {
  issuer: "https://proxy.ingenieria.usac.edu.gt/autenticacion/oauth2",
  scopes: ["openid", "profile"],
  /* This is the CLIENT_ID generated from a Firebase project */
  clientId: "App Portal",
};

let StorageKey = "2dfefef2fd6df51aff00cf4d48897c31";

export async function signInAsync() {
  let authState = await AppAuth.authAsync(config);
  await cacheAuthAsync(authState);
  console.log("signInAsync", authState);
  return authState;
}

async function cacheAuthAsync(authState) {
  return await AsyncStorage.setItem(StorageKey, JSON.stringify(authState));
}

export async function getCachedAuthAsync() {
  let value = await AsyncStorage.getItem(StorageKey);
  let authState = JSON.parse(value);
  console.log("getCachedAuthAsync", authState);
  if (authState) {
    if (checkIfTokenExpired(authState)) {
      return refreshAuthAsync(authState);
    } else {
      return authState;
    }
  }
  return null;
}

function checkIfTokenExpired({ accessTokenExpirationDate }) {
  return new Date(accessTokenExpirationDate) < new Date();
}

async function refreshAuthAsync({ refreshToken }) {
  let authState = await AppAuth.refreshAsync(config, refreshToken);
  console.log("refreshAuth", authState);
  await cacheAuthAsync(authState);
  return authState;
}

export async function signOutAsync({ accessToken }) {
  try {
    await AppAuth.revokeAsync(config, {
      token: accessToken,
      isClientIdProvided: true,
    });
    await AsyncStorage.removeItem(StorageKey);
    return null;
  } catch (e) {
    alert(`Failed to revoke token: ${e.message}`);
  }
}
export default Login;
