import React, { useState, useEffect } from "react";
import HeaderNavigation from "./HeaderNavigation";
import { View, ScrollView, Dimensions, StatusBar, Alert } from "react-native";
import { Card, Header, Button, Text, Avatar } from "react-native-elements";
import { useDispatch } from "react-redux";
import { GetMenu, SetMenu1 } from "../redux/actions/ActionCreators";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
//import { NavigationActions, StackActions } from "react-navigation";
import {
  faBars,
  faSignOutAlt,
  faUserCheck,
} from "@fortawesome/free-solid-svg-icons";
import Loading from "./Loading";
const screenHeight = Dimensions.get("window").height;
import { useIsFocused } from "@react-navigation/native";
const FocusAwareStatusBar = (props) => {
  const isFocused = useIsFocused();

  return isFocused ? <StatusBar {...props} /> : null;
};

function Home({ MenuActive, navigation }) {
  const [LogOff, setLogOff] = useState(true);
  const hasUnsavedChanges = true;
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  //dispatch(SetMenu1());

  useEffect(() => {
    setTimeout(() => {
      isFocused ? dispatch(SetMenu1()) : "";
    }, 500);
    navigation.addListener("beforeRemove", (e) => {
      if (!LogOff) {
        // Si estamos logeados no regresar
        return;
        // Prompt the user before leaving the screen
      }
      e.preventDefault();
      Alert.alert("¿Cerrar Sesión?", "", [
        {
          text: "No",
          style: "cancel",
          onPress: () => {
            navigation.navigate("AppDrawer");
          },
        },
        {
          text: "Si",
          style: "destructive",
          // If the user confirmed, then we dispatch the action we blocked earlier
          // This will continue the action that had triggered the removal of the screen
          onPress: () => navigation.dispatch(e.data.action),
        },
      ]);
    });
  });

  return (
    <View style={{ height: screenHeight }}>
      <FocusAwareStatusBar barStyle="ligth-content" backgroundColor="#4A4138" />
      <HeaderNavigation navigation={navigation} />
      <ScrollView>
        <Card style={{ alignItems: "center" }}>
          <Card.Title>DATOS ESTUDIANTILES</Card.Title>
          <Text style={{ textAlign: "center" }}>
            Información Personal y Académica
          </Text>
          <Card.Divider />
          <Text style={{ marginBottom: 10, textAlign: "center" }}>
            Consulta toda tu informacion sobre cursos aprobados, asignados,
            laboratorios, repitencia, inscripciones, etc. Modifica tu
            informacion personal y de contacto.
          </Text>
          <Button
            buttonStyle={{
              borderRadius: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}
            title="Ir a Datos Estudiantiles"
            onPress={() => navigation.navigate("DatosEstudiantiles")}
          />
        </Card>
        <Card style={{ alignItems: "center" }}>
          <Card.Title>ASIGNACIONES</Card.Title>
          <Text style={{ textAlign: "center" }}>
            Semestre Regular, Vacaciones y Retrasadas
          </Text>
          <Card.Divider />

          <Text style={{ marginBottom: 10, textAlign: "center" }}>
            Realiza aquí tu proceso de asignación en línea
          </Text>
          <Button
            buttonStyle={{
              borderRadius: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}
            title="Ir a Asignaciones"
            onPress={() => navigation.navigate("Asignaciones")}
          />
        </Card>
      </ScrollView>
    </View>
  );
}

export default Home;
