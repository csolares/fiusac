import { StyleSheet, Dimensions, StatusBar } from "react-native";
const SCREEN_WIDTH = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
export default StyleSheet.create({
  header: {
    backgroundColor: "#4A4138",
  },
  headerButton: { backgroundColor: "#4A4138", color: "white" },
  headerIcon: { backgroundColor: "#4A4138", color: "white" },
  item2: {
    fontFamily: "serif",
    fontSize: 18,
    marginBottom: 5,
  },
  dataTitle: {
    fontFamily: "serif",
    color: "gray",
    fontSize: 14,
  },
  title: {
    fontFamily: "serif",
    color: "gray",
  },
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 10,
    marginBottom: 10,
  },
  itemContainer: {
    height: 70,
    width: SCREEN_WIDTH,
    backgroundColor: "white",
    justifyContent: "center",
    padding: 16,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    fontFamily: "serif",
    fontSize: 18,
    marginBottom: 5,
  },

  scrollView: {
    backgroundColor: "white",
  },
  separatorLine: {
    height: 1,
    backgroundColor: "black",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 100,
      height: 200,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  cancelButton: {
    backgroundColor: "#ff033b",
  },
  acceptButton: {
    backgroundColor: "#00933f",
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  logo: {
    width: 250,
    height: 45,
    resizeMode: "stretch",
  },

  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  loginLogo: {
    width: 300,
    height: 80,
    resizeMode: "stretch",
  },
  loginAndroidCard: {
    elevation: 0,
    borderColor: "transparent",
  },
  loginIOS: {
    shadowColor: "rgba(0,0,0, .2)",
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0, //default is 1
    shadowRadius: 0, //default is 1
  },
  loginButton: {
    backgroundColor: "#808080",
    padding: 20,
  },
  loginCheckbox: {
    borderColor: "transparent",
    backgroundColor: "white",
  },
  loginBottomView: {
    position: "absolute",
    bottom: 0,
    marginBottom: screenHeight * 0.05,
    flexDirection: "row",
  },
  loginResetPass: {
    color: "blue",
    marginLeft: SCREEN_WIDTH * 0.1,
    marginRight: SCREEN_WIDTH * 0.1,
  },
  loginExternosURL: {
    color: "blue",
    marginLeft: SCREEN_WIDTH * 0.1,
    marginRight: SCREEN_WIDTH * 0.1,
  },
  loginExternosResetPass: {
    color: "blue",
    alignItems: "center",
    alignSelf: "center",
  },
  loginBottomViewExterno: {
    bottom: 0,
    marginBottom: screenHeight * 0.05,
  },
  loadingView: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  loadingText: {
    color: "#512DA8",
    fontSize: 14,
    fontWeight: "bold",
  },
});
