import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "./Login";

import AppDrawer from "./AppDrawer";

import Home from "./Home";
const Stack = createStackNavigator();

function LoginStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="AppDrawer"
        component={AppDrawer}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

export default LoginStack;
