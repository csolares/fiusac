import React, { useCallback }  from "react"
import { View, Dimensions } from "react-native"
import { Text } from "react-native-elements"
import Unorderedlist from "react-native-unordered-list"
import HeaderNavigation from "../HeaderNavigation"
import { Linking, TouchableOpacity } from "react-native"
const screenHeight = Dimensions.get("window").height

const OpenURLText = ({ url, texto }) => {
    const handlePress = useCallback(async () => {
      const supported = await Linking.canOpenURL(url)
      if (supported) {
        await Linking.openURL(url)
      } else {
        Alert.alert(`Imposible abrir esta URL: ${url}`)
      }
    }, [url])
  
    return (
        <TouchableOpacity> 
            <Text h3 h3Style={{fontStyle:"italic", textAlign:"justify"}} style={{ color: "blue", textAlign:"center" }} onPress={handlePress}>
                {texto}
            </Text> 
        </TouchableOpacity>
    )
}

function Links({ navigation }) {
  return (
    <View style={{ height: screenHeight }}>
        <HeaderNavigation navigation={navigation} />
        <View style={{ display: "flex", flexDirection: "column", flex: 1, flexWrap: "wrap", height: screenHeight, alignItems:"center"}}>
          <Text h1 style={{marginTop: (screenHeight/4), marginBottom:10}}> Asignación de Cursos  </Text>

          
          <Text h2 h2Style={{fontWeight:"normal", textAlign:"justify", marginBottom:10, marginTop:10, marginRight: 10}}> Asigna tus cursos para los siguientes periodos: </Text>

          <Unorderedlist color="black" style={{fontSize:30}}>
              <OpenURLText url={"https://www.youtube.com/watch?v=DWz9fyqoULY&feature=youtu.be"} texto={"Video tutorial de Asignación de Semestre Regular"} />
          </Unorderedlist>
          <Unorderedlist color="black" style={{fontSize:30}}>
              <OpenURLText url={"https://www.youtube.com/watch?v=oZBdvMgjFkA&feature=youtu.be"} texto={"Video tutorial de Asignación Escuela de Vacaciones"}/>
          </Unorderedlist>
        </View>
    </View>
  )
}

export default Links
