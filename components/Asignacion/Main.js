import React, { useState } from "react";
import {
  createDrawerNavigator
} from "@react-navigation/drawer";
const Drawer = createDrawerNavigator();
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faHourglassStart,
  faSpellCheck,
  faSync,
  faLaptopHouse
} from "@fortawesome/free-solid-svg-icons";

import Home from "../Home";
import Tutoriales from "./Tutoriales"
import SemestreMain from "./Semestre/SemestreMain"
function MainMenu() {
  return (
    <Drawer.Navigator initialRouteName="links">
      <Drawer.Screen
        name="Inicio"
        component={Home}
        options={{
          drawerLabel: "Inicio",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faLaptopHouse} />,
        }}
      />
      <Drawer.Screen
        name="Semestre"
        component={SemestreMain}
        options={{
          drawerLabel: "Semestre",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faHourglassStart} />,
        }}
      />
      <Drawer.Screen
        name="links"
        component={Tutoriales}
        options={{
          drawerLabel: () => null
        }}
      />
    </Drawer.Navigator>
  );
}

export default MainMenu;