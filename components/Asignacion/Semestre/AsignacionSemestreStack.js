import React, { useState } from "react";
import { createStackNavigator } from "@react-navigation/stack";

import SeleccionCursos from "../Comunes/SeleccionCursos";
import SeleccionSecciones from "../Comunes/SeleccionSecciones";
import VistaDiferencial from "../Comunes/VistaDiferencial";
const Stack = createStackNavigator();

function AsignacionSemestreStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SeleccionCursos"
        component={SeleccionCursos}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SeleccionSecciones"
        component={SeleccionSecciones}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="VistaDiferencial"
        component={VistaDiferencial}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

export default AsignacionSemestreStack;
