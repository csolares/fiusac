import React from "react";
import { View, Dimensions } from "react-native";
import { Text } from "react-native-elements";
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import HeaderNavigation from "../../HeaderNavigation";
import { Linking, StyleSheet, TouchableOpacity } from "react-native";
const screenHeight = Dimensions.get("window").height;


const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: 'chartreuse' },
    text: { margin: 6 }
  });


var DATOS = {
    tableHead: ['Numero', 'Link Descarga'],
    tableData: [['1', 'Link'], ['1', 'Link']]
}

function Links({ navigation }) {
  return (
    
    <View style={{ display: "flex", flexDirection: "column", flex: 1, flexWrap: 'wrap', margin: 10 , height: screenHeight}}>
        <HeaderNavigation navigation={navigation} />
            <Table borderStyle={{borderWidth: 2, borderColor: 'forestgreen'}}>
                <Row data={DATOS.tableHead} style={styles.head} textStyle={styles.text}/>
                <Rows data={DATOS.tableData} textStyle={styles.text}/>
            </Table>
    </View>
  );
}

export default Links;
