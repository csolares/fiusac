import React, { useState } from "react";
import { View, ScrollView, Dimensions, Alert } from "react-native";
import { Button, Text, Divider} from "react-native-elements";
import DropDownPicker from "react-native-dropdown-picker";
import HeaderNavigation from "../../HeaderNavigation";
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

function SeleccionSecciones({ route, navigation}) {

    const { lista } = route.params;
    const [recorrido, setRecorrido] = useState(0);
    const [seccionClase, setSeccionClase] = useState();
    const [seccionLaboratorio, setSeccionLaboratorio] = useState();
    const [seccionExtra, setSeccionExtra] = useState();

    const funcionAtras = () => {
        actualizarDatos();
        if (recorrido === 0) {
            navigation.navigate("SeleccionCursos")
        } else {
            setRecorrido(recorrido - 1);
        }
    }

    const funcionSiguiente = () => {
        actualizarDatos();
        if (recorrido === (lista.length - 1) ) {
            navigation.navigate("VistaDiferencial", {lista: lista})
            //navigation.navigate("VistaDiferencial")
        } else {
            setRecorrido(recorrido + 1);
        }
    }
    
    const actualizarDatos = () => {
        lista[recorrido].seccionesSeleccionadas[0] = seccionClase 
        lista[recorrido].seccionesSeleccionadas[1] = seccionLaboratorio
        lista[recorrido].seccionesSeleccionadas[2] = seccionExtra
    }

    //actualizarDatos();

    return (
        <View style={{ height: '100%' }}>
        <HeaderNavigation navigation={navigation} />
            <View style={{
                paddingLeft: 25,
                paddingRight: 25,
                flex: 1,
                justifyContent: 'flex-end',
                marginBottom: 20 
                }}
            >

                <View style={{height:'100%'}}>
                    <Text h2 style={{marginTop:((screenHeight/2) - 300) ,textAlign:"center"}}>{`${lista[recorrido].id} - ${lista[recorrido].title}`}</Text>

                    <DropDownPicker
                        items={lista[recorrido].secciones}
                        defaultValue={"Seccion de Clase Magistras"}
                        containerStyle={{ height: 40, margin:20}}
                        style={{ backgroundColor: "#fafafa" }}
                        itemStyle={{
                            justifyContent: "flex-start",
                        }}
                        dropDownStyle={{ backgroundColor: "#fafafa" }}
                        onChangeItem={(item) => {
                            setSeccionClase(item.id)
                        }}
                    />

                    <DropDownPicker
                        items={lista[recorrido].seccionesLab}
                        defaultValue={"Seccion de Laboratorio"}
                        containerStyle={{ height: 40, margin:20}}
                        style={{ backgroundColor: "#fafafa" }}
                        itemStyle={{
                            justifyContent: "flex-start",
                        }}
                        dropDownStyle={{ backgroundColor: "#fafafa" }}
                        onChangeItem={(item) => {
                            setSeccionLaboratorio(item.id)
                        }}
                    />

                    <DropDownPicker
                        items={lista[recorrido].seccionesExtra}
                        defaultValue={"Seccion Extra"}
                        containerStyle={{ height: 40, margin:20}}
                        style={{ backgroundColor: "#fafafa" }}
                        itemStyle={{
                            justifyContent: "flex-start",
                        }}
                        dropDownStyle={{ backgroundColor: "#fafafa" }}
                        onChangeItem={(item) => {
                            setSeccionExtra(item.id)
                        }}
                    />
                </View>

                <View style={{
                        flexDirection: 'row'
                    }}>
                    <Button
                        buttonStyle={{
                        marginTop:10,
                        width: (screenWidth/2) - 30,
                        borderRadius: 10,
                        maxHeight: '100%'
                        }}   
                        title={ recorrido === 0 ? "Seleccion de Cursos" : "Atras"}
                        onPress={() => { 
                            funcionAtras() 
                        }}
                    />
                    <Divider style={{margin:5}}/>
                    <Button
                        buttonStyle={{
                        marginTop:10,
                        width: (screenWidth/2) - 30,
                        borderRadius: 10,
                        maxHeight: '100%'
                        }}
                        title={ (recorrido === (lista.length - 1) ) ? "Confirmar" : "Siguiente"}
                        onPress={() => 
                            funcionSiguiente()
                        }
                    />
                </View>
            </View>
        </View>
    );
}

export default SeleccionSecciones;
