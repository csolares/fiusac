import React, { useState } from "react";
import { View } from "react-native";
import { Card, Text } from "react-native-elements";
import DropDownPicker from "react-native-dropdown-picker";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { 
    faTimesCircle 
} from "@fortawesome/free-solid-svg-icons";
import {Picker} from '@react-native-picker/picker';
import { Divider } from "react-native-elements";
function tarjetaCurso({ curso }) {
    const [seccion, setSeccion] = useState("Seccion de laboratorio");
    const [language, setLanguage] = useState("java");
    return (
        <Card  containerStyle={{borderRadius:15, 
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 4,
            },
            shadowOpacity: 0.32,
            shadowRadius: 5.46,

            elevation: 9,}}
        >
            <Card.Title style={{fontSize:25}}>
                <Text>{curso.title}</Text>
                <FontAwesomeIcon icon={faTimesCircle} size={20} /> 
            </Card.Title>
            <Card.Divider style={{backgroundColor:"black"}}/>
            <View 
                style={{
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: "row",
                    marginBottom: 20,
                    alignContent:"center"
                }}
            >
                <View>
                    <Text style={{borderBottomColor:"black", borderBottomWidth:1}}>Seccion</Text>
                    <Picker
                        selectedValue={language}
                        style={{height: 50, width: 100}}
                        onValueChange={(itemValue, itemIndex) =>
                            setLanguage(itemValue)
                        }>
                        <Picker.Item label="Java" value="java" />
                        <Picker.Item label="JavaScript" value="js" />
                    </Picker>
                </View>
                <View style={{borderLeftColor:"gray", borderLeftWidth: 1, margin:10}}/>
                <View>
                    <Text>Seccion</Text>
                    <Picker
                        selectedValue={language}
                        style={{height: 50, width: 100}}
                        onValueChange={(itemValue, itemIndex) =>
                            setLanguage(itemValue)
                        }>
                        <Picker.Item label="Java" value="java" />
                        <Picker.Item label="JavaScript" value="js" />
                    </Picker>
                </View>
            </View>
        </Card>    
    );
}

export default tarjetaCurso;