import React from "react";
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { View, Dimensions } from "react-native";
import { Divider, Text, Button } from "react-native-elements";
import HeaderNavigation from "../../HeaderNavigation";
import { StyleSheet } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
const screenHeight = Dimensions.get("window").height;


const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#f1f8ff' },
    head2: { height: 40, backgroundColor: 'red' },
    text: { margin: 6 }
  });

var DATOS = {
    tableHead: ['Curso', 'Seccion', 'Laboratorio', 'Practica'],
    tableData: []
}

function Links({ route, navigation }) {
    const { lista } = route.params

    lista.forEach(element => {
        DATOS.tableData.push( 
            [ element.title, element.seccionesSeleccionadas[0], element.seccionesSeleccionadas[1], element.seccionesSeleccionadas[2] ] 
        )  
    });

    return (
        <View style={{ height: screenHeight}}>
            <HeaderNavigation navigation={navigation} />
            <ScrollView>
                <Text h4> Cursos Sin Problemas</Text>
                <Divider />

                <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                    <Row data={DATOS.tableHead} style={styles.head} textStyle={styles.text}/>
                    <Rows data={DATOS.tableData} textStyle={styles.text}/>
                </Table>

                <Text h4> Cursos Con Problemas</Text>
                <Divider />

                <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                    <Row data={DATOS.tableHead} style={styles.head2} textStyle={styles.text}/>
                    <Rows data={DATOS.tableData} textStyle={styles.text}/>
                </Table>
                
                <Divider />
            
                <Button
                    buttonStyle={{
                    borderRadius: 0,
                    marginLeft: 0,
                    marginRight: 0,
                    marginBottom: 0,
                    }}
                    title="Confirmar"
                    onPress={() => navigation.navigate("Constancia")}
                />
                <Divider />
                <Button
                    buttonStyle={{
                    borderRadius: 0,
                    marginLeft: 0,
                    marginRight: 0,
                    marginBottom: 0,
                    }}
                    title="Regresar a seleccion de Cursos"
                    onPress={() => navigation.popToTop()}
                />
            </ScrollView>
        </View>
        
    );
}

export default Links;


/*

 

 
*/