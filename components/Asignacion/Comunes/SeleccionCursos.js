import React, { useState } from "react";
import { View, ScrollView, Dimensions, FlatList, TouchableOpacity, Alert } from "react-native";
import { Button, Text, Divider, Card } from "react-native-elements";
import DropDownPicker from "react-native-dropdown-picker";
import HeaderNavigation from "../../HeaderNavigation";
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faTimes
} from "@fortawesome/free-solid-svg-icons";

var cursosSeleccionados = []


var DATA = [
  {
    id: "017",
    title: "Social Humanistica 1",
    label: "017 - Social Humanistica 1",
    value: "017 - Social Humanistica 1",
    secciones: [ 
      {
        id: "Seccion de Clase Magistras",
        value: "Seccion de Clase Magistras",
        label: "Seccion de Clase Magistras"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "C",
        label: "C",
        value: "C"
      },
      {
        id: "D",
        label: "D",
        value: "D"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesLab: [
        {
        id: "Seccion de Laboratorio",
        value: "Seccion de Laboratorio",
        label: "Seccion de Laboratorio"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesExtra: [
        {
        id: "Seccion Extra",
        value: "Seccion Extra",
        label: "Seccion Extra"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      }
  ],  
    seccionesSeleccionadas: ["-", "-", "-"]
  },
  {
    id: "101",
    title: "Matematica Basica 1",
    secciones: [
        {
        id: "Seccion de Clase Magistras",
        value: "Seccion de Clase Magistras",
        label: "Seccion de Clase Magistras"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "C",
        label: "C",
        value: "C"
      },
      {
        id: "D",
        label: "D",
        value: "D"
      },
      {
        id: "E",
        label: "E",
        value: "E"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesLab: [
        {
        id: "Seccion de Laboratorio",
        value: "Seccion de Laboratorio",
        label: "Seccion de Laboratorio"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "C",
        label: "C",
        value: "C"
      }
    ],
    seccionesExtra: [
        {
        id: "Seccion Extra",
        value: "Seccion Extra",
        label: "Seccion Extra"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      }
    ],
    label: "101 - Matematica Basica 1",
    value: "101 - Matematica Basica 1",
    seccionesSeleccionadas: ["-", "-", "-"]
  },
  {
    id: "003",
    title: "Orientacion y Liderazgo",
    secciones: [
        {
        id: "Seccion de Clase Magistras",
        value: "Seccion de Clase Magistras",
        label: "Seccion de Clase Magistras"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesLab: [
        {
        id: "Seccion de Laboratorio",
        value: "Seccion de Laboratorio",
        label: "Seccion de Laboratorio"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesExtra: [
        {
        id: "Seccion Extra",
        value: "Seccion Extra",
        label: "Seccion Extra"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    label: "003 - Orientacion y Liderazgo",
    value: "003 - Orientacion y Liderazgo",
    seccionesSeleccionadas: ["-", "-", "-"]
  },
  {
    id: "069",
    title: "Tecnica Complementaria",
    secciones: [
        {
        id: "Seccion de Clase Magistras",
        value: "Seccion de Clase Magistras",
        label: "Seccion de Clase Magistras"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      }
    ],
    seccionesLab: [
        {
        id: "Seccion de Laboratorio",
        value: "Seccion de Laboratorio",
        label: "Seccion de Laboratorio"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      }
    ],
    seccionesExtra: [
        {
        id: "Seccion Extra",
        value: "Seccion Extra",
        label: "Seccion Extra"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      }
    ],
    label: "069 - Tecnica Complementaria",
    value: "069 - Tecnica Complementaria",
    seccionesSeleccionadas: ["-", "-", "-"]
  },
  {
    id: "039",
    title: "Deportes 1",
    secciones: [
        {
        id: "Seccion de Clase Magistras",
        value: "Seccion de Clase Magistras",
        label: "Seccion de Clase Magistras"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "C",
        label: "C",
        value: "C"
      },
      {
        id: "D",
        label: "D",
        value: "D"
      },
      {
        id: "E",
        label: "E",
        value: "E"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesLab: [
        {
        id: "Seccion de Laboratorio",
        value: "Seccion de Laboratorio",
        label: "Seccion de Laboratorio"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesExtra: [
        {
        id: "Seccion Extra",
        value: "Seccion Extra",
        label: "Seccion Extra"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    label: "039 - Deportes 1",
    value: "039 - Deportes 1",
    seccionesSeleccionadas: ["-", "-", "-"]
  },
  {
    id: "348",
    title: "Quimica General 1",
    secciones: [
        {
        id: "Seccion de Clase Magistras",
        value: "Seccion de Clase Magistras",
        label: "Seccion de Clase Magistras"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "C",
        label: "C",
        value: "C"
      },
      {
        id: "D",
        label: "D",
        value: "D"
      }
    ],
    seccionesLab: [
        {
        id: "Seccion de Laboratorio",
        value: "Seccion de Laboratorio",
        label: "Seccion de Laboratorio"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "C",
        label: "C",
        value: "C"
      },
      {
        id: "D",
        label: "D",
        value: "D"
      }
      ],
      seccionesExtra: [
          {
        id: "Seccion Extra",
        value: "Seccion Extra",
        label: "Seccion Extra"
      },
        {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "C",
        label: "C",
        value: "C"
      },
      {
        id: "D",
        label: "D",
        value: "D"
      }
    ],
    label: "348 - Quimica General 1",
    value: "348 - Quimica General 1",
    seccionesSeleccionadas: ["-", "-", "-"]
  },
  {
    id: "700",
    title: "Compiladores",
    secciones: [
        {
        id: "Seccion de Clase Magistras",
        value: "Seccion de Clase Magistras",
        label :"Seccion de Clase Magistras"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesLab: [
        {
        id: "Seccion de Laboratorio",
        value: "Seccion de Laboratorio",
        label: "Seccion de Laboratorio"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    seccionesExtra: [
        {
        id: "Seccion Extra",
        value: "Seccion Extra",
        label :"Seccion Extra"
      },
      {
        id: "A",
        label: "A",
        value: "A"
      },
      {
        id: "B",
        label: "B",
        value: "B"
      },
      {
        id: "N",
        label: "N",
        value: "N"
      }
    ],
    label: "700 - Compiladores",
    value: "700 - Compiladores",
    seccionesSeleccionadas: ["-", "-", "-"]
  }
];

const Item = ({ item, onPress }, props) => {
  return (
    <TouchableOpacity onPress={onPress} containerStyle={{ width:screenWidth }}>
      <View style={{
                    backgroundColor: "white",
                    justifyContent: "space-between",
                    padding: 10,
                    flexDirection: "row",
                    marginBottom: 20 
                  }}>
        <Text containerStyle={{ width:10 }}>
          {`${item.id} - ${item.title}`}
        </Text>
        <FontAwesomeIcon  icon={faTimes} />
      </View>
    </TouchableOpacity>
  );
};

function SeleccionCursos({ navigation }) {

  const [curso, setCurso] = useState();
  const [listaCurso, setListaCurso] = useState(cursosSeleccionados);


  const agregarCurso = (cursoAgregar) => {

    var array = [...listaCurso]; 
    var index = array.indexOf(cursoAgregar)
    if (index === -1) {
      setListaCurso([ ...listaCurso, cursoAgregar]);
    } else {
      Alert.alert(
        'Curso Repetido',
        `Ya ha agregado este curso:
        ${cursoAgregar.id} - ${cursoAgregar.title}`,
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') }
        ],
        { cancelable: false }
      );
    }
  }

  const avanzar = () => {
    if (listaCurso.length === 0){
      Alert.alert(
        'Error',
        `Debe de seleccionar al menos un curso`,
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') }
        ],
        { cancelable: false }
      );
    } else {
      navigation.navigate("SeleccionSecciones", {lista: listaCurso})
    }
  }

  //const [selectedId, setSelectedId] = useState(null);



  const onPress = (cursoBorrar) => {
    //setSelectedId(cursoBorrar);
    var array = [...listaCurso]; 
    var index = array.indexOf(cursoBorrar)
    if (index !== -1) {
      array.splice(index, 1);
      setListaCurso( array );
    }
  };

  const renderCurso = ({ item }) => {
    return (
      <Item
        item={item}
        onPress={() => onPress(item)}
      />
    );
  };

  return (
    <View style={{ height: '100%' }}>
      <HeaderNavigation navigation={navigation} />
      <View 
        style={{
          paddingLeft: 25,
          paddingRight: 25,
          flex: 1,
          justifyContent: 'flex-end',
          marginBottom: 20 
        }}
      >
        <Text h2 style={{textAlign:"center"}}>Seleccion de Cursos </Text>
        <DropDownPicker
          items={DATA}
          placeholder= "Curso que desea asignar"
          containerStyle={{ height: 40, margin:20}}
          style={{ backgroundColor: "#fafafa" }}
          itemStyle={{
            justifyContent: "flex-start",
          }}
          dropDownStyle={{ backgroundColor: "#fafafa" }}
          onChangeItem={(item) => {
            setCurso(item);
          }}
        />
        <Divider />
        
          {
            listaCurso.length === 0 ?      
            <ScrollView>
              <Card>
                <FlatList
                ListHeaderComponent={
                  <>
                    <Text h4 style={{textAlign:"center", fontWeight:"normal"}}>Inicio de lista</Text>
                    <Card.Divider style={{backgroundColor:"black", height:5, borderRadius:10}}/>
                  </>
                }
                />
              </Card>
            </ScrollView>
            :
            <ScrollView>
              <Card>
                <FlatList
                ListHeaderComponent={
                  <>
                    <Text h4 style={{textAlign:"center", fontWeight:"normal"}}>Inicio de lista</Text>
                    <Card.Divider style={{backgroundColor:"black", height:5, borderRadius:10}}/>
                  </>
                }
                  data={listaCurso}
                  renderItem={renderCurso}
                  keyExtractor={(item) => item.id}
                  extraData={(item) => item.id}
                  ItemSeparatorComponent={() => {
                    return <Card.Divider style={{backgroundColor:"black"}}/>;
                  }}
                  ListFooterComponent={
                    <>
                      <Card.Divider style={{backgroundColor:"black"}}/>
                    </>
                  }
                />
              </Card>
            </ScrollView>
          }
        
        <View style={{
          flexDirection: 'row'
        }}>
          <Button
            buttonStyle={{
              marginTop:10,
              width: (screenWidth/2) - 30,
              borderRadius: 10,
              maxHeight: '100%'
            }}
            title="Agregar Curso"
            onPress={() => { 
              agregarCurso(curso) 
            }}
          />
          <Divider style={{margin:5}}/>
          <Button
            buttonStyle={{
              marginTop:10,
              width: (screenWidth/2) - 30,
              borderRadius: 10,
              maxHeight: '100%'
            }}
            title={"Confirmar Seleccion"}
            onPress={() => {
              avanzar()
            }}
          />
        </View>
      </View>
    </View>
  );
}

export default SeleccionCursos;
