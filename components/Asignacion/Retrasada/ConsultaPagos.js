import React from "react";
import { View, ScrollView, Dimensions } from "react-native";
import { Card, Button, Text } from "react-native-elements";

import HeaderNavigation from "../../HeaderNavigation";
const screenHeight = Dimensions.get("window").height;
function AsignacionSuficiencia({ navigation }) {
  return (
    <View style={{ height: screenHeight }}>
      <HeaderNavigation navigation={navigation} />
      <ScrollView>
        <Card style={{ alignItems: "center" }}>
          <Card.Title>Matematica Basica 1</Card.Title>
          <Text style={{ textAlign: "center" }}>
            Codigo 1
          </Text>
          <Card.Divider />
          <Text style={{ marginBottom: 10, textAlign: "center" }}>
            Seccion A
          </Text>
          <Button
            buttonStyle={{
              borderRadius: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}
            title="Asignar retrasada"
            onPress={() => navigation.navigate("DatosEstudiantiles")}
          />
        </Card>
        <Card style={{ alignItems: "center" }}>
          <Card.Title>Matematica Basica 2</Card.Title>
          <Text style={{ textAlign: "center" }}>
            Codigo 2
          </Text>
          <Card.Divider />

          <Text style={{ marginBottom: 10, textAlign: "center" }}>
            Seccion N
          </Text>
          <Button
            buttonStyle={{
              borderRadius: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}
            title="Asignar retrasada"
            onPress={() => navigation.navigate("Asignaciones")}
          />
        </Card>

        <Card style={{ alignItems: "center" }}>
          <Card.Title>Fisica Basica 1</Card.Title>
          <Text style={{ textAlign: "center" }}>
            Codigo 2
          </Text>
          <Card.Divider />

          <Text style={{ marginBottom: 10, textAlign: "center" }}>
            Seccion N
          </Text>
          <Button
            buttonStyle={{
              borderRadius: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}
            title="Asignar retrasada"
            onPress={() => navigation.navigate("Asignaciones")}
          />
        </Card>

        <Card style={{ alignItems: "center" }}>
          <Card.Title>Fisica Basica 2</Card.Title>
          <Text style={{ textAlign: "center" }}>
            Codigo 2
          </Text>
          <Card.Divider />

          <Text style={{ marginBottom: 10, textAlign: "center" }}>
            Seccion N
          </Text>
          <Button
            buttonStyle={{
              borderRadius: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}
            title="Asignar retrasada"
            onPress={() => navigation.navigate("Asignaciones")}
          />
        </Card>

        <Card style={{ alignItems: "center" }}>
          <Card.Title>Logica</Card.Title>
          <Text style={{ textAlign: "center" }}>
            Codigo 2
          </Text>
          <Card.Divider />

          <Text style={{ marginBottom: 10, textAlign: "center" }}>
            Seccion N
          </Text>
          <Button
            buttonStyle={{
              borderRadius: 0,
              marginLeft: 0,
              marginRight: 0,
              marginBottom: 0,
            }}
            title="Asignar retrasada"
            onPress={() => navigation.navigate("Asignaciones")}
          />
        </Card>
      </ScrollView>
    </View>
  );
}

export default AsignacionSuficiencia;
