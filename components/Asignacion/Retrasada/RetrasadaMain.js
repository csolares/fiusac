import React, { useState } from "react";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Dimensions } from "react-native";
import GeneracionOrden from "./GeneracionOrden";
import ConsultaPagos from "./ConsultaPagos"
import Ionicons from "react-native-vector-icons/Ionicons";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { ScrollView } from "react-native";
const screenHeight = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
const Tab = createBottomTabNavigator();

function LaboratorioMain({ navigation }) {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === "ConsultaPagos") {
            iconName = focused ? "ios-flask" : "ios-flask";
          } else if (route.name === "GeneracionOrden") {
            iconName = focused ? "ios-download" : "ios-download";
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: "tomato",
        inactiveTintColor: "gray",
      }}
    >
      <Tab.Screen name="ConsultaPagos" component={ConsultaPagos} />
      <Tab.Screen name="GeneracionOrden" component={GeneracionOrden} />
    </Tab.Navigator>
  );
}

export default LaboratorioMain;
