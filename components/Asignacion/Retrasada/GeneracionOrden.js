import React from "react";
import { View, Dimensions } from "react-native";
import { Text } from "react-native-elements";
import { DataTable } from 'react-native-paper';
import HeaderNavigation from "../../HeaderNavigation";
import { Linking, StyleSheet, TouchableOpacity } from "react-native";
const screenHeight = Dimensions.get("window").height;
function Links({ navigation }) {
  return (
    <View style={{ display: "flex", flexDirection: "column", flex: 1, flexWrap: 'wrap', margin: 10 , height: screenHeight}}>
        <HeaderNavigation navigation={navigation} />
        <DataTable>
            <DataTable.Header>
                <DataTable.Title>Orden de Pago</DataTable.Title>
                <DataTable.Title>Estado</DataTable.Title>
                <DataTable.Title>Descarga</DataTable.Title>
            </DataTable.Header>

            <DataTable.Row>
                <DataTable.Cell>boleta retrasada</DataTable.Cell>
                <DataTable.Cell>Pagada</DataTable.Cell>
                <DataTable.Cell>
                    <TouchableOpacity>
                            <Text style={{ color: 'gray' }} onpress={() => Linking.openURL('https://www.google.com')} >
                                    link
                            </Text>
                    </TouchableOpacity>
                </DataTable.Cell>
            </DataTable.Row>

            <DataTable.Row>
                <DataTable.Cell>boleta retrasada 2</DataTable.Cell>
                <DataTable.Cell>Sin pagar</DataTable.Cell>
                <DataTable.Cell>
                    <TouchableOpacity>
                            <Text style={{ color: 'blue' }} onpress={() => Linking.openURL('https://www.google.com')} >
                                    link
                            </Text>
                    </TouchableOpacity>
                </DataTable.Cell>
            </DataTable.Row>

            <DataTable.Pagination
                page={1}
                numberOfPages={3}
                onPageChange={page => {
                    console.log(page);
                }}
                label="1-2 of 6"
            />
        </DataTable>
    </View>
  );
}

export default Links;