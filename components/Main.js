import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import {
  View,
  ToastAndroid,
  Platform,
  AlertIOS,
  Dimensions,
  StatusBar,
} from "react-native";
import * as NetInfo from "@react-native-community/netinfo";
import LoginStack from "./LoginStack";
const screenHeight = Dimensions.get("window").height;

function Main() {
  const unsubscribe = NetInfo.addEventListener((state) => {
    console.log("Connection type", state.type);
    console.log("Is connected?", state.isConnected);
  });
  unsubscribe();
  useEffect(() => {
    NetInfo.fetch().then((state) => {
      console.log("Connection type", state.type);
      if (Platform.OS === "android") {
        state.isConnected
          ? ToastAndroid.showWithGravity(
              "Conectado a " + state.type,
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM
            )
          : ToastAndroid.showWithGravity(
              "No Conectado",
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM
            );
      } else {
        /*        state.isConnected
          ? AlertIOS.alert("FIUSAC", "Conectado")
          : AlertIOS.alert("FIUSAC", "No Conectado");*/
      }
    });
  }, []);
  return (
    <View style={{ height: screenHeight }}>
      <NavigationContainer>
        <LoginStack />
      </NavigationContainer>
    </View>
  );
}

export default Main;
