import React, { useState } from "react";

import { ActivityIndicator, Dimensions, Text, View } from "react-native";

const screenHeight = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
import styles from "./styles";
function Loading() {
  return (
    <View style={styles.loadingView}>
      <ActivityIndicator size="large" color="#512DA8" />
      <Text style={styles.loadingText}>Loading . . .</Text>
    </View>
  );
}

export default Loading;
