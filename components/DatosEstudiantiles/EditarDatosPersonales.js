import React, { useState } from "react";
import { SafeAreaView, View, TextInput, Dimensions } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import styles from "../styles";
import { Text, Card, Divider, Button } from "react-native-elements";
import HeaderNavigation from "../HeaderNavigation";

const SCREEN_WIDTH = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

function EditarDatosPersonales({ navigation }) {
  const [modalVisible, setModalVisible] = useState(false);
  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);
  };
  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };
  return (
    <SafeAreaView>
      <View style={{ height: screenHeight, width: SCREEN_WIDTH }}>
        <HeaderNavigation navigation={navigation} />
        <Card>
          <Card.Title h4 style={styles.title}>
            Editar Datos Personales
          </Card.Title>
          <Card.Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>Dirección</Text>
            <TextInput
              style={(styles.item2, { width: SCREEN_WIDTH })}
              placeholder="Dirección"
            />
          </View>
          <Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>Colonia</Text>
            <TextInput
              placeholder="Colonia"
              style={(styles.item2, { width: SCREEN_WIDTH })}
            />
          </View>
          <Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>Fecha de Nacimiento</Text>

            <TextInput
              value={date.toLocaleDateString("en-GB")}
              onFocus={showDatepicker}
              style={(styles.item2, { width: SCREEN_WIDTH })}
            />
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={"date"}
                display="default"
                onChange={onChange}
              />
            )}
          </View>
          <Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>Correo Electronico</Text>
            <TextInput
              placeholder="Correo Electronico"
              style={(styles.item2, { width: SCREEN_WIDTH })}
            />
          </View>
          <Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>Teléfono de Domicilio</Text>
            <TextInput
              placeholder="Teléfono de Domicilio"
              style={(styles.item2, { width: SCREEN_WIDTH })}
            />
          </View>
          <Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>Teléfono Celular</Text>
            <TextInput
              placeholder="Teléfono Celular"
              style={(styles.item2, { width: SCREEN_WIDTH })}
            />
            <View style={{ flexDirection: "row" }}>
              <Button
                title={"Actualizar Datos"}
                buttonStyle={styles.acceptButton}
              />
              <Button
                title={"Cancelar"}
                buttonStyle={styles.cancelButton}
                onPress={() => {
                  navigation.navigate("VerDatosPersonales");
                }}
              />
            </View>
          </View>
        </Card>
      </View>
    </SafeAreaView>
  );
}

export default EditarDatosPersonales;
