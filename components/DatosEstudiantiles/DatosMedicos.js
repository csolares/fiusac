import React, { useState } from "react";
import {
  ScrollView,
  View,
  Dimensions,
  TextInput,
  StatusBar,
} from "react-native";
import { Card, Text, Button } from "react-native-elements";
import styles from "../styles";
import HeaderNavigation from "../HeaderNavigation";
import DropDownPicker from "react-native-dropdown-picker";

import { useIsFocused } from "@react-navigation/native";
const SCREEN_WIDTH = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const data = {
  nombre: "Jorge Eduardo Vega Paxtor",
  correo: "eljj@gmail.com",
  telCasa: "22413254",
  telCel: "32015784",
  direccion: "3ra calle 3-23 San miguel Petapa",
  fechaNac: "01/01/1994",
  registroAca: "201214285",
  dpi: "275489870101",
};

function DatosMedicos({ navigation }) {
  const [telEmergencia, setTelEmergencia] = useState(data.telCel);
  const [nombreEmergencia, setnombreEmergencia] = useState(data.nombre);
  const [capacidadesEspeciales, setcapacidadesEspeciales] = useState(
    "Seleccione una opción"
  );
  const [enfCronica, setenfCronica] = useState("Seleccione una opción");
  const [tipoSangre, settipoSangre] = useState("Seleccione una opción");
  const [alergico, setalergico] = useState("    ");
  const FocusAwareStatusBar = (props) => {
    const isFocused = useIsFocused();
    return isFocused ? <StatusBar {...props} /> : null;
  };
  return (
    <View style={{ height: screenHeight }}>
      <HeaderNavigation navigation={navigation} />
      <FocusAwareStatusBar barStyle="ligth-content" backgroundColor="#4A4138" />
      <ScrollView style={{ backgroundColor: "white" }}>
        <Card style={{ height: screenHeight }}>
          <Card.Title h4 style={styles.title}>
            Datos Personales de Salud
          </Card.Title>

          <Card.Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>Teléfono de Emergencia</Text>
          </View>
          <TextInput
            style={(styles.item2, { width: SCREEN_WIDTH })}
            onChangeText={(text) => setTelEmergencia(text)}
            value={telEmergencia}
          />
          <Card.Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>
              Nombre de Contacto de Emergencia
            </Text>
          </View>
          <TextInput
            style={(styles.item2, { width: SCREEN_WIDTH })}
            onChangeText={(text) => setnombreEmergencia(text)}
            value={nombreEmergencia}
          />
          <Card.Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>
              ¿Posees capacidades especiales?
            </Text>
          </View>
          <DropDownPicker
            items={[
              {
                label: "Seleccione una opción",
                value: "Seleccione una opción",
              },
              {
                label: "SI",
                value: "SI",
              },
              {
                label: "NO",
                value: "NO",
              },
            ]}
            defaultValue={"Seleccione una opción"}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: "#fafafa" }}
            itemStyle={{
              justifyContent: "flex-start",
            }}
            dropDownStyle={{ backgroundColor: "#fafafa" }}
            onChangeItem={(item) => setcapacidadesEspeciales(item)}
          />

          <Card.Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>
              ¿Padeces alguna enfermedad crónica?
            </Text>
          </View>
          <DropDownPicker
            items={[
              {
                label: "Seleccione una opción",
                value: "Seleccione una opción",
              },
              {
                label: "NINGUNA",
                value: "NINGUNA",
              },
              {
                label: "ENFERMEDAD PULMONAR OBSTRUCTIVA CRÓNICA",
                value: "ENFERMEDAD PULMONAR OBSTRUCTIVA CRÓNICA",
              },
              {
                label: "DIABETES",
                value: "DIABETES",
              },
              {
                label: "PARKINSON",
                value: "PARKINSON",
              },
              {
                label: "ALZHEIMER",
                value: "ALZHEIMER",
              },
              {
                label: "ESCLEROSIS MÚLTIPLE",
                value: "ESCLEROSIS MÚLTIPLE",
              },

              {
                label: "ENFERMEDADES CARDIOVASCULARES",
                value: "ENFERMEDADES CARDIOVASCULARES",
              },

              {
                label: "CÁNCER",
                value: "CÁNCER",
              },

              {
                label: "HIPERTENSIÓN",
                value: "HIPERTENSIÓN",
              },

              {
                label: "LUMBALGIA",
                value: "LUMBALGIA",
              },

              {
                label: "COLESTEROL",
                value: "COLESTEROL",
              },

              {
                label: "DEPRESIÓN",
                value: "DEPRESIÓN",
              },

              {
                label: "ANSIEDAD",
                value: "ANSIEDAD",
              },

              {
                label: "TIROIDES",
                value: "TIROIDES",
              },

              {
                label: "OESTEOPOROSIS",
                value: "OESTEOPOROSIS",
              },
              {
                label: "OTRA",
                value: "OTRA",
              },
            ]}
            defaultValue={"Seleccione una opción"}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: "#fafafa" }}
            itemStyle={{
              justifyContent: "flex-start",
            }}
            dropDownStyle={{ backgroundColor: "#fafafa" }}
            onChangeItem={(item) => setenfCronica(item)}
          />
          <Card.Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>Tipo de Sangre</Text>
          </View>
          <DropDownPicker
            items={[
              {
                label: "Seleccione una opción",
                value: "Seleccione una opción",
              },
              {
                label: "O Negativo",
                value: "O Negativo",
              },
              {
                label: "O Positivo",
                value: "O Positivo",
              },
              {
                label: "A Negativo",
                value: "A Negativo",
              },
              {
                label: "A Positivo",
                value: "A Positivo",
              },
              {
                label: "B Negativo",
                value: "B Negativo",
              },
              {
                label: "B Positivo",
                value: "B Positivo",
              },
              {
                label: "AB Negativo",
                value: "AB Negativo",
              },
              {
                label: "AB Positivo",
                value: "AB Positivo",
              },
              {
                label: "No sé",
                value: "No sé",
              },
            ]}
            defaultValue={"Seleccione una opción"}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: "#fafafa" }}
            itemStyle={{
              justifyContent: "flex-start",
            }}
            dropDownStyle={{ backgroundColor: "#fafafa" }}
            onChangeItem={(item) => settipoSangre(item)}
          />

          <Card.Divider />
          <View style={{ alignItems: "baseline" }}>
            <Text style={styles.dataTitle}>
              ¿Eres alérgico a algún medicamento?
            </Text>
          </View>
          <TextInput
            style={(styles.item2, { width: SCREEN_WIDTH })}
            onChangeText={(text) => setalergico(text)}
            value={alergico}
          />
          <Card.Divider />
          <Button
            title={"Actualizar Datos"}
            buttonStyle={styles.acceptButton}
          />
        </Card>
      </ScrollView>
    </View>
  );
}

export default DatosMedicos;
