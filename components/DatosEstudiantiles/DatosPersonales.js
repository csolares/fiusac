import React, { useEffect } from "react";

import styles from "../styles";
import { View, ScrollView, SafeAreaView, Dimensions } from "react-native";
import { Card, Button, Text, Avatar } from "react-native-elements";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import HeaderNavigation from "../HeaderNavigation";
import { useDispatch } from "react-redux";
import { SetMenu2 } from "../../redux/actions/ActionCreators";

const SCREEN_WIDTH = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const theme = {
  colors: {
    primary: "blue",
    success: "green",
  },
};

const data = {
  nombre: "Jorge Eduardo Vega Paxtor",
  correo: "eljj@gmail.com",
  telCasa: "22413254",
  telCel: "32015784",
  direccion: "3ra calle 3-23 San miguel Petapa",
  fechaNac: "01/01/1994",
  registroAca: "201214285",
  dpi: "275489870101",
};

function DatosPersonales({ navigation }) {
  const dispatch = useDispatch();
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      // do something

      dispatch(SetMenu2());
      setTimeout(() => {
        //navigation.navigate("Profile");
      }, 5);
    });

    return unsubscribe;
  }, [navigation]);
  /*

  useEffect(() => {
    
  }, []);*/
  return (
    <SafeAreaView>
      <View style={{ height: screenHeight }}>
        <HeaderNavigation navigation={navigation} />

        <ScrollView style={{ backgroundColor: "white" }}>
          <Card>
            <Card.Title h4 style={styles.title}>
              Datos Personales
            </Card.Title>
            <Card.Divider />
            <View
              style={{
                alignContent: "center",
                alignItems: "center",

                marginBottom: 10,
              }}
            >
              <Avatar
                rounded
                size="xlarge"
                activeOpacity={0.7}
                source={{
                  uri:
                    "https://images.vexels.com/media/users/3/145908/preview2/52eabf633ca6414e60a7677b0b917d92-creador-de-avatar-masculino.jpg",
                }}
              />
            </View>
            <View>
              <Button
                icon={<FontAwesomeIcon icon={faPencilAlt} />}
                buttonStyle={{
                  backgroundColor: "green",
                }}
                onPress={() => navigation.navigate("EditarDatosP")}
                title={"Editar Datos Personales"}
              />
            </View>
            <Card.Divider />
            <View style={{ alignItems: "baseline" }}>
              <Text style={styles.dataTitle}>Nombre</Text>
              <Text style={styles.item2}>{data.nombre}</Text>
            </View>
            <Card.Divider />
            <View style={{ alignItems: "baseline" }}>
              <Text style={styles.dataTitle}>Correo</Text>
              <Text style={styles.item2}>{data.correo}</Text>
            </View>
            <Card.Divider />
            <View style={{ alignItems: "baseline" }}>
              <Text style={styles.dataTitle}>Registro Académico</Text>
              <Text style={styles.item2}>{data.registroAca}</Text>
            </View>
            <Card.Divider />
            <View style={{ alignItems: "baseline" }}>
              <Text style={styles.dataTitle}>DPI</Text>
              <Text style={styles.item2}>{data.dpi}</Text>
            </View>
            <Card.Divider />
            <View style={{ alignItems: "baseline" }}>
              <Text style={styles.dataTitle}>Fecha de Nacimiento</Text>
              <Text style={styles.item2}>{data.fechaNac}</Text>
            </View>
            <Card.Divider />
            <View style={{ alignItems: "baseline" }}>
              <Text style={styles.dataTitle}>Teléfono Domicilio</Text>
              <Text style={styles.item2}>{data.telCasa}</Text>
            </View>
            <Card.Divider />
            <View style={{ alignItems: "baseline" }}>
              <Text style={styles.dataTitle}>Teléfono Celular</Text>
              <Text style={styles.item2}>{data.telCel}</Text>
            </View>
            <Card.Divider />
            <View style={{ alignItems: "baseline" }}>
              <Text style={styles.dataTitle}>Dirección</Text>
              <Text style={styles.item2}>{data.direccion} </Text>
            </View>
          </Card>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

export default DatosPersonales;
