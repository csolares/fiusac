import React, { useState } from "react";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from "@react-navigation/drawer";
import MiCuenta from "./MiCuenta";
const Drawer = createDrawerNavigator();
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faList,
  faUser,
  faHome,
  faUserGraduate,
  faClock,
  faFlask,
  faFileAlt,
} from "@fortawesome/free-solid-svg-icons";
import Home from "../Home";


function DEstDrawer() {
  return (
    <Drawer.Navigator initialRouteName="Profile">
      <Drawer.Screen
        name="Inicio"
        component={Home}
        options={{
          drawerLabel: "Inicio",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faHome} />,
        }}
      />
      <Drawer.Screen
        name="Profile"
        component={MiCuenta}
        options={{
          drawerLabel: "Mi Cuenta",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faUser} />,
        }}
      />
      {/*<Drawer.Screen
        name="Cursos"
        component={Cursos}
        options={{
          drawerLabel: "Cursos",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faList} />,
        }}
      />
      <Drawer.Screen
        name="Notas"
        component={NotasStack}
        options={{
          drawerLabel: "Notas",
          drawerIcon: ({ focused }) => (
            <FontAwesomeIcon icon={faUserGraduate} />
          ),
        }}
      />
      <Drawer.Screen
        name="Horarios"
        component={HorariosStack}
        options={{
          drawerLabel: "Horarios",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faClock} />,
        }}
      />
      <Drawer.Screen
        name="Laboratorios"
        component={Laboratorios}
        options={{
          drawerLabel: "Laboratorios",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faFlask} />,
        }}
      />
      <Drawer.Screen
        name="DocElectronicos"
        component={DocElectronicos}
        options={{
          drawerLabel: "Documentos Electronicos",
          drawerIcon: ({ focused }) => <FontAwesomeIcon icon={faFileAlt} />,
        }}
      />*/}
    </Drawer.Navigator>
  );
}

export default DEstDrawer;
