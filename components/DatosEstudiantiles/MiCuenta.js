import React, { useState } from "react";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Dimensions } from "react-native";
import DatosPersonales from "./DatosPersonales";
import DatosMedicos from "./DatosMedicos";
import Ionicons from "react-native-vector-icons/Ionicons";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import EditarDatosPersonales from "./EditarDatosPersonales";

const screenHeight = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

function DatosPersonalesStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="VerDatosPersonales"
        component={DatosPersonales}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="EditarDatosP"
        component={EditarDatosPersonales}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

function MiCuentaTab({ navigation }) {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === "DatosPersonales") {
            iconName = focused ? "ios-person" : "ios-person";
          } else if (route.name === "DatosMedicos") {
            iconName = focused ? "ios-medical" : "ios-medical";
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: "tomato",
        inactiveTintColor: "gray",
      }}
    >
      <Tab.Screen name="DatosPersonales" component={DatosPersonalesStack} />
      <Tab.Screen name="DatosMedicos" component={DatosMedicos} />
    </Tab.Navigator>
  );
}

export default MiCuentaTab;
