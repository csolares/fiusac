import React, { useState, useEffect } from "react";
import { Header, Button } from "react-native-elements";
import { Dimensions, Image, View, Platform, StatusBar } from "react-native";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faBars, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
const screenHeight = Dimensions.get("window").height;
import styles from "./styles";
import { useIsFocused } from "@react-navigation/native";
const FocusAwareStatusBar = (props) => {
  const isFocused = useIsFocused();
  return isFocused ? <StatusBar {...props} /> : null;
};
function HeaderNavigation({ navigation }) {
  const [LogOff, setLogOff] = useState(true);
  const hasUnsavedChanges = true;

  return (
    <>
      <FocusAwareStatusBar barStyle="dark-content" backgroundColor="#ecf0f1" />
      <Header
        placement="center"
        containerStyle={styles.header}
        leftComponent={
          <Button
            onPress={() => navigation.toggleDrawer()}
            icon={<FontAwesomeIcon icon={faBars} style={styles.headerIcon} />}
            accessibilityLabel="Abrir Menú"
            buttonStyle={styles.headerButton}
          />
        }
        centerComponent={
          <Image
            style={styles.logo}
            source={require("../assets/img/logofiusac.png")}
          />
        }
        rightComponent={
          <Button
            onPress={() =>
              navigation.reset({
                routes: [{ name: "Login" }],
              })
            }
            icon={
              <FontAwesomeIcon icon={faSignOutAlt} style={styles.headerIcon} />
            }
            buttonStyle={styles.headerButton}
            accessibilityLabel="Cerrar Sesión"
          />
        }
      />
    </>
  );
}

export default HeaderNavigation;
