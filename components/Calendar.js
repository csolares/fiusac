import React from "react";
import { View, Dimensions } from "react-native";
import { Card, Header, Button, Text } from "react-native-elements";
import { Calendar, CalendarList, Agenda } from "react-native-calendars";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faBars, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import HeaderNavigation from "./HeaderNavigation";
const screenHeight = Dimensions.get("window").height;
function CalendarComponent({ navigation }) {
  return (
    <View style={{ height: screenHeight }}>
      <HeaderNavigation navigation={navigation} />
      <Card>
        <Card.Title>Calendar</Card.Title>
        <Card.Divider />
        <Calendar />
      </Card>
    </View>
  );
}

export default CalendarComponent;
