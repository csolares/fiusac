import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import MenuReducer from "./reducers/Menus";
import AsyncStorage from "@react-native-async-storage/async-storage";
/*import { persistStore, persistReducer } from "redux-persist";
const persistConfig = {
  key: "root",
  storage: AsyncStorage,
};*/
const rootReducer = combineReducers({
  //MenuReducer: persistReducer(persistConfig, MenuReducer),
  MenuReducer: MenuReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk, logger));
//export const persistor = persistStore(store);
