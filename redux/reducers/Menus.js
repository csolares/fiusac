import * as ActionTypes from "../actions/ActionTypes";

function MenuReducer(
  state = { errMess: null, isLoading: false, Menu: 1 },
  action
) {
  switch (action.type) {
    case ActionTypes.ADD_MENU:
      return {
        ...state,
        isLoading: false,
        errMess: null,
        Menu: action.payload,
      };

    case ActionTypes.MENU_LOADING:
      return { ...state, isLoading: true, errMess: null, Menu: 1 };

    case ActionTypes.MENU_FAILED:
      return { ...state, isLoading: true, errMess: action.payload, Menu: 1 };

    default:
      return state;
  }
}
export default MenuReducer;
