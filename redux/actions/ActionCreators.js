import * as ActionTypes from "./ActionTypes";

export const SetMenu1 = () => (dispatch) => {
  dispatch(MenuLoading());
  return dispatch(addMenu(1));
};
export const SetMenu2 = () => (dispatch) => {
  dispatch(MenuLoading());
  return dispatch(addMenu(2));
};
export const SetMenu3 = () => (dispatch) => {
  dispatch(MenuLoading());
  return dispatch(addMenu(3));
};

export const GetMenu = () => (dispatch) => {
  dispatch(MenuLoading());
  return dispatch(addMenu(Menu));
};

export const addMenu = (menu) => ({
  type: ActionTypes.ADD_MENU,
  payload: menu,
});

export const MenuLoading = () => ({
  type: ActionTypes.MENU_LOADING,
});
export const MenuFailed = (errmess) => ({
  type: ActionTypes.MENU_FAILED,
  payload: errmess,
});
